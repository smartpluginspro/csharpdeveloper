﻿namespace Koko.Core.Infrastructure.Models
{
    public record RoleDto(int Id, string Name);
}