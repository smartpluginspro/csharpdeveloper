﻿using System.Threading;
using System.Threading.Tasks;
using Koko.Core.Application.Services.Abstract;
using Koko.Core.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Koko.Core.Infrastructure
{
    public class PizzaContext : DbContext, IPizzaContext
    {
        public readonly IConfiguration _configuration;

        public const string DEFAULT_SCHEMA = "koko";

        public PizzaContext(DbContextOptions<PizzaContext> options, IConfiguration configuration) : base(options)
        {
            _configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder.IsConfigured == false)
            {
                optionsBuilder
                    .UseNpgsql(_configuration.GetConnectionString("PostgreSql"))
                    .UseLazyLoadingProxies();

                base.OnConfiguring(optionsBuilder);
            }
        }

        public DbSet<Unit> Units { get; set; }

        public DbSet<Ingredient> Ingredients { get; set; }

        public DbSet<ProductComposition> ProductCompositions { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<ProductType> ProductTypes { get; set; }

        public DbSet<MenuItemProduct> MenuItemProducts { get; set; }

        public DbSet<MenuItem> MenuItems { get; set; }

        public DbSet<Menu> Menus { get; set; }

        public DbSet<OrderItem> OrderItems { get; set; }

        public DbSet<OrderStatus> OrderStatuses { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<Delivery> Deliveries { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<Role> UserRoles { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(PizzaContext).Assembly);
        }

        

    }
}