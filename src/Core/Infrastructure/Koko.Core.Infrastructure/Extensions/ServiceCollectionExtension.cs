﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Koko.Core.Application.Services.Abstract;
using Koko.Core.Application.Services.Concrete;
using Koko.Core.Infrastructure.MapperProfiles;

namespace Koko.Core.Infrastructure.Extensions
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services)
        {
            services.AddDbContext<PizzaContext>();
            services.AddScoped<IPizzaContext>(provider => provider.GetService<PizzaContext>());
            services.AddScoped<IRoleService, RoleService>();
            services.AddAutoMapper(options => options.AddProfile(new DtoProfile()));
        
            return services;
        }
    }
}
