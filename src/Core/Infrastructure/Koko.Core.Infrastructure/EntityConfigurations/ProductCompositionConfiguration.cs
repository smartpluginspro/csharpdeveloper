﻿using Koko.Core.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Koko.Core.Infrastructure.EntityConfigurations
{
    public class ProductCompositionConfiguration : IEntityTypeConfiguration<ProductComposition>
    {
        public void Configure(EntityTypeBuilder<ProductComposition> builder)
        {
            builder.HasOne(x => x.Product)
                    .WithMany(x => x.Composition)
                    .HasForeignKey(x => x.ProductId)
                    .IsRequired(false)
                    .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(x => x.Ingredient)
                .WithMany()
                .HasForeignKey(x => x.IngredientId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
