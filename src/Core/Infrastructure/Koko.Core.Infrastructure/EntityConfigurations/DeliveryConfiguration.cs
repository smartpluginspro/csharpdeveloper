﻿using Koko.Core.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Koko.Core.Infrastructure.EntityConfigurations
{
    public class DeliveryConfiguration : IEntityTypeConfiguration<Delivery>
    {
        public void Configure(EntityTypeBuilder<Delivery> builder)
        {
            builder.HasOne(x => x.Order)
                    .WithOne(x => x.Delivery)
                    .HasForeignKey<Delivery>(x => x.OrderId)
                    .IsRequired()
                    .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(x => x.Courier)
                    .WithMany()
                    .HasForeignKey(x => x.CourierId)
                    .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
