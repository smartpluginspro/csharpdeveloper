﻿using Koko.Core.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Koko.Core.Infrastructure.EntityConfigurations
{
    public class MenuItemProductConfiguration : IEntityTypeConfiguration<MenuItemProduct>
    {
        public void Configure(EntityTypeBuilder<MenuItemProduct> builder)
        {
            builder.HasOne(x => x.MenuItem)
                    .WithMany(x => x.Products)
                    .IsRequired()
                    .HasForeignKey(x => x.MenuItemId)
                    .OnDelete(DeleteBehavior.SetNull);

            builder.HasOne(x => x.Product)
                .WithMany(x => x.MenuItems)
                .IsRequired()
                .HasForeignKey(x => x.ProductId)
                .OnDelete(DeleteBehavior.SetNull);
        }
    }
}
