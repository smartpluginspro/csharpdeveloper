﻿using Koko.Core.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Koko.Core.Infrastructure.EntityConfigurations
{
    public class MenuItemConfiguration : IEntityTypeConfiguration<MenuItem>
    {
        public void Configure(EntityTypeBuilder<MenuItem> builder)
        {
            builder.HasOne(x => x.Menu)
                    .WithMany(x => x.Items)
                    .HasForeignKey(x => x.MenuId)
                    .IsRequired()
                    .OnDelete(DeleteBehavior.Restrict);

            builder.HasIndex(x => x.SortOrder);
        }
    }
}
