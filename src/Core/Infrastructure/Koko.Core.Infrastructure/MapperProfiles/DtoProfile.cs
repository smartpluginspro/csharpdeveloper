﻿using AutoMapper;
using Koko.Core.Domain.Models;
using Koko.Core.Infrastructure.Models;

namespace Koko.Core.Infrastructure.MapperProfiles
{
    public class DtoProfile : Profile
    {
        public DtoProfile()
        {
            CreateMap<Role, RoleDto>().ReverseMap();
        }
    }
}