﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Koko.Core.Application.Services.Abstract;
using Koko.Core.Domain.Models;
using Koko.Core.Infrastructure.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Koko.Core.Api.Controllers
{
    public class RolesController : BaseController
    {
        private readonly IRoleService _roleService;
        private readonly IMapper _mapper;
        private readonly ILogger<RolesController> _logger;

        public RolesController(IRoleService roleService, IMapper mapper, ILogger<RolesController> logger)
        {
            _roleService = roleService;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<RoleDto>>> List(CancellationToken cancellationToken)
        {
            var roles = await _roleService.GetRoles(50, 0, cancellationToken);
            return Ok(_mapper.Map<IEnumerable<RoleDto>>(roles));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<RoleDto>> Get(int id, CancellationToken cancellationToken)
        {
            var role = await _roleService.Find(id, cancellationToken);
            if (role == null)
            {
                return NotFound(id);
            }

            return Ok(_mapper.Map<RoleDto>(role));
        }

        [HttpPost]
        public async Task<ActionResult<RoleDto>> Create(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return BadRequest("Role name cannot be null");
            }

            try
            {
                var role = await _roleService.Create(name);
                return Created($"/Roles/Get/{role.Id}", _mapper.Map<RoleDto>(role));
            }
            catch (ArgumentException ex)
            {
                _logger.LogWarning(ex.Message, name);
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                var affectedEntities = await _roleService.Delete(id);
                return Ok(affectedEntities);
            }
            catch (ArgumentException ex)
            {
                _logger.LogWarning(ex.Message, id);
                return NotFound(id);
            }
        }

        [HttpPut]
        public async Task<ActionResult<RoleDto>> Update(RoleDto model)
        {
            if (model == null)
            {
                return BadRequest("Model cannot be empty");
            }

            try
            {
                var role = _mapper.Map<Role>(model);
                var result = await _roleService.Update(role);
                return _mapper.Map<RoleDto>(result);
            }
            catch (ArgumentException ex)
            {
                _logger.LogWarning(ex.Message, model);
                return NotFound(model.Id);
            } 
        }
    }
}