﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Koko.Core.Application.Services.Abstract;
using Koko.Core.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Koko.Core.Application.Services.Concrete
{
    public class RoleService : IRoleService
    {
        private readonly IPizzaContext _context;

        public RoleService(IPizzaContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Role>> GetRoles(int take, int skip, CancellationToken cancellationToken)
        {
            return await _context.UserRoles.Skip(skip).Take(take).ToListAsync(cancellationToken);
        }

        public async Task<Role> Find(int id, CancellationToken cancellationToken)
        {
            return await _context.UserRoles.FirstOrDefaultAsync(x => x.Id == id, cancellationToken);
        }

        public async Task<Role> Create(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException(nameof(name));
            }

            var existing = await _context.UserRoles.AnyAsync(x => x.Name == name);
            if (existing)
            {
                throw new ArgumentException($"Role {name} already exists");
            }

            var role = new Role
            {
                Name = name
            };
            _context.UserRoles.Add(role);
            await _context.SaveChangesAsync();
            
            return role;
        }

        public async Task<int> Delete(int id)
        {
            var role = await _context.UserRoles.FirstOrDefaultAsync(x => x.Id == id);
            if (role == null)
            {
                throw new ArgumentException("Role not found");
            }

            _context.UserRoles.Remove(role);
            return await _context.SaveChangesAsync();
        }

        public async Task<Role> Update(Role role)
        {
            if (role == null)
            {
                throw new ArgumentNullException(nameof(role));
            }
            
            var existingRole = await _context.UserRoles.FirstOrDefaultAsync(x => x.Id == role.Id);
            if (existingRole == null)
            {
                throw new ArgumentException(nameof(role.Id));
            }

            existingRole.Name = role.Name;
            await _context.SaveChangesAsync();
            return existingRole;
        }
    }
}