﻿using System.Threading;
using System.Threading.Tasks;
using Koko.Core.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Koko.Core.Application.Services.Abstract
{
    public interface IPizzaContext
    {
        DbSet<Role> UserRoles { get; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }
}
