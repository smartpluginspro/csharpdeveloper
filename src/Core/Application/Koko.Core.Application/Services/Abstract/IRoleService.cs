﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Koko.Core.Domain.Models;

namespace Koko.Core.Application.Services.Abstract
{
    public interface IRoleService
    {
        Task<IEnumerable<Role>> GetRoles(int take, int skip, CancellationToken cancellationToken);

        Task<Role> Find(int id, CancellationToken cancellationToken);
        
        Task<Role> Create([NotNull] string name);

        Task<int> Delete(int id);

        Task<Role> Update([NotNull] Role role);
    }
}