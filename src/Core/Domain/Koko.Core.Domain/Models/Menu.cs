﻿using Koko.Core.Domain.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Koko.Core.Domain.Models
{
    public class Menu : Entity<int>
    {
        [Column("name")]
        public string Name { get; set; }

        [Column("available")]
        public bool Available { get; set; }

        public virtual ICollection<MenuItem> Items { get; set; }

    }
}