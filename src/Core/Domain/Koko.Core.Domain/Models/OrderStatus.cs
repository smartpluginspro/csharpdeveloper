﻿using Koko.Core.Domain.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Koko.Core.Domain.Models
{
    public class OrderStatus : Entity<int>
    {
        [Column("name")]
        public string Name { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
    }
}