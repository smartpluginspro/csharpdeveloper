﻿using Koko.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Koko.Core.Domain.Models
{
    public class Order : Entity<int>
    {
        [Column("id_Customer")]
        public int CustomerId { get; set; }

        public virtual User Customer { get; set; }

        [Column("id_Operator")]
        public int OperatorId { get; set; }

        public virtual User Operator { get; set; }

        [Column("creation_date")]
        public DateTimeOffset CreationDate { get; set; }

        [Column("completion_date")]
        public DateTimeOffset CompletionDate { get; set; }

        [Column("id_OrderStatus")]
        public int OrderStatusId { get; set; }

        public virtual OrderStatus Status { get; set; }

        [Column("cost")]
        public decimal Cost { get; set; }

        public virtual Delivery Delivery { get; set; }

        public virtual ICollection<OrderItem> Items { get; set; }
    }
}