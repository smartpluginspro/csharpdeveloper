﻿using Koko.Core.Domain.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Koko.Core.Domain.Models
{
    public class Product : Entity<int>
    {
        [Column("name")]
        public string Name { get; set; }

        [Column("id_ProductType")]
        public int ProductTypeId { get; set; }

        public virtual ICollection<ProductComposition> Composition { get; set; }
        public virtual ProductType ProductType { get; set; }

        public virtual ICollection<MenuItemProduct> MenuItems { get; set; }
    }
}