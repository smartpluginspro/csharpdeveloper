﻿using Koko.Core.Domain.Common;
using System.ComponentModel.DataAnnotations.Schema;

namespace Koko.Core.Domain.Models
{
    public class OrderItem : Entity<int>
    {
        [Column("id_Order")]
        public int OrderId { get; set; }

        public virtual Order Order { get; set; }

        [Column("id_MenuItem")]
        public int MenuItemId { get; set; }

        public virtual MenuItem MenuItem { get; set; }

        [Column("cost")]
        public decimal Cost { get; set; }
    }
}