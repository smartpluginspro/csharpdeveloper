﻿using Koko.Core.Domain.Common;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Koko.Core.Domain.Models
{
    public class User : Entity<int>
    {
        [Column("first_name")]
        public string Name { get; set; }

        [Column("middle_name")]
        public string MiddleName { get; set; }

        [Column("last_name")]
        public string Surname { get; set; }

        [Column("phone")]
        public string Phone { get; set; }

        [Column("registration_date")]
        public DateTimeOffset RegistrationDate { get; set; }

        [Column("last_online")]
        public DateTimeOffset LastOnline { get; set; }

        [Column("id_Role")]
        public int RoleId { get; set; }

        public virtual Role Role { get; set; }
    }
}