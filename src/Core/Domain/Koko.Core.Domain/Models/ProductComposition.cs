﻿using Koko.Core.Domain.Common;
using System.ComponentModel.DataAnnotations.Schema;

namespace Koko.Core.Domain.Models
{
    public class ProductComposition : Entity<int>
    {
        [Column("id_Product")]
        public int ProductId { get; set; }

        public virtual Product Product { get; set; }

        [Column("id_Ingredient")]
        public int IngredientId { get; set; }

        public virtual Ingredient Ingredient { get; set; }

        [Column("count")]
        public int Count { get; set; }
    }
}