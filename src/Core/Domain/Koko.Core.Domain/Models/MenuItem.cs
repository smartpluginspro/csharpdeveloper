﻿using Koko.Core.Domain.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Koko.Core.Domain.Models
{
    public class MenuItem : Entity<int>
    {
        [Column("id_Menu")]
        public int MenuId { get; set; }

        public virtual Menu Menu { get; set; }

        [Column("name")]
        public string Name { get; set; }

        [Column("cost")]
        public decimal Cost { get; set; }

        [Column("sort_order")]
        public int SortOrder { get; set; }

        public virtual ICollection<MenuItemProduct> Products { get; set; }
    }
}