﻿using Koko.Core.Domain.Common;
using System.ComponentModel.DataAnnotations.Schema;

namespace Koko.Core.Domain.Models
{
    public class MenuItemProduct : Entity<int>
    {
        [Column("id_MenuItem")]
        public int MenuItemId { get; set; }

        public virtual MenuItem MenuItem { get; set; }

        [Column("id_Product")]
        public int ProductId { get; set; }

        public virtual Product Product { get; set; }


    }
}