﻿using Koko.Core.Domain.Common;
using System.ComponentModel.DataAnnotations.Schema;

namespace Koko.Core.Domain.Models
{
    public class Ingredient : Entity<int>
    {
        [Column("cost")]
        public decimal Cost { get; set; }

        [Column("calories")]
        public int Calories { get; set; }

        [Column("id_Unit")]
        public int UnitId { get; set; }

        public virtual Unit Unit { get; set; }
    }
}