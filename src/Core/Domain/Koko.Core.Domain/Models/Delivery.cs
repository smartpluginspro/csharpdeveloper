﻿using Koko.Core.Domain.Common;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Koko.Core.Domain.Models
{
    public class Delivery : Entity<int>
    {
        [Column("id_Order")]
        public int OrderId { get; set; }

        public virtual Order Order { get; set; }

        [Column("arrived_date")]
        public DateTimeOffset ArrivedDate { get; set; }

        [Column("pickup_date")]
        public DateTimeOffset PickUpDate { get; set; }

        [Column("id_courier")]
        public int CourierId { get; set; }

        public virtual User Courier { get; set; }
    }
}