﻿using Koko.Core.Domain.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Koko.Core.Domain.Models
{
    public class Role : Entity<int>
    {
        [Column("name")]
        public string Name { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}