﻿using Koko.Core.Domain.Common;
using System.ComponentModel.DataAnnotations.Schema;

namespace Koko.Core.Domain.Models
{
    public class ProductType : Entity<int>
    {
        [Column("name")]
        public string Name { get; set; }
    }
}